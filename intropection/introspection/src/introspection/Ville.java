package introspection;

public class Ville {
	 
	  private String nomVille;
	  private String nomPays;
	  private int nbreHabitants;
	  private char categorie;
	   
	  public Ville(){         
	    nomVille = "Inconnu";
	    nomPays = "Inconnu";
	    nbreHabitants = 0;
	  }
	 
	  public Ville(String pNom, int pNbre, String pPays)
	  {
	    nomVille = pNom;
	    nomPays = pPays;
	    nbreHabitants = pNbre;
	  }  
	    
	  //Retourne le nom de la ville
	  public String getNom()  {  
	    return nomVille;
	  }

	  //Retourne le nom du pays
	  public String getNomPays()
	  {
	    return nomPays;
	  }

	  // Retourne le nombre d'habitants
	  public int getNombreHabitants()
	  {
	    return nbreHabitants;
	  } 

	  //Retourne la cat�gorie de la ville
	  public char getCategorie()
	  {
	    return categorie;
	  } 
	 
	  //D�finit le nom de la ville
	  public void setNom(String pNom)
	  {
	    nomVille = pNom;
	  }

	  //D�finit le nom du pays
	  public void setNomPays(String pPays)
	  {
	    nomPays = pPays;
	  }
	 

	  //Retourne la description de la ville
	  public String decrisToi(){
	    return "\t"+this.nomVille+" est une ville de "+this.nomPays+ ", elle comporte : "+this.nbreHabitants+" habitant(s)";
	  }

	}