package introspection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.JavaClass;


public class Main {

	public static void main(String[] args) {
		
			
		/************* Acc�s aux �l�ments d'une classe � partir d'un .class � l'aide de la librairie "bcel" *************/
		
			/*Changer le chemin d'acc�s aux fichiers .class*/
			String file_name = "C:/Users/romain/Desktop/UML_Reverse/intropection/introspection/bin/introspection/Ville.class";
			try {
				ClassParser classparser = new ClassParser(file_name) ;
				JavaClass inputClass = classparser.parse() ;
				
				System.out.println("*******Package*********");
				System.out.println(inputClass.getPackageName());
				System.out.println();
				
				System.out.println("*******Class Name*********");
				System.out.println(inputClass.getClassName()) ;
				System.out.println();
			
		        System.out.println("*******Fields*********");
		        System.out.println(Arrays.toString(inputClass.getFields()));
		        System.out.println();
		        
		        System.out.println("*******Methods with attributes*********");
		        System.out.println(Arrays.toString(inputClass.getMethods()));
		        System.out.println();
		        
		        System.out.println("*******Derivated class*********");
		        
		        System.out.println(inputClass.getSuperclassName());
		        System.out.println();
		        
			}
			catch (Exception e) {
				e.printStackTrace() ;
			} 
			
			
		
		    /************* Acc�s aux �l�ments d'une classe � partir du code *************/
			
			/*
			Class c = new Ville().getClass();
			
			Method[] m = c.getDeclaredMethods();
						                
			System.out.println("Il y a " + m.length + " m�thodes dans cette classe");
			//On parcourt le tableau de m�thodes
			for(int i = 0; i < m.length; i++)
			System.out.println(m[i]);
			   
			
			Field[] f = c.getDeclaredFields();
			
			System.out.println("Il y a " + f.length + " champs dans cette classe");
			//On parcourt le tableau de m�thodes
			for(int i = 0; i < f.length; i++)
				System.out.println(f[i].getName());
			*/
	}
}

