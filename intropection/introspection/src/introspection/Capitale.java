package introspection;

public class Capitale extends Ville {
    
	  protected Monument monument = new Monument();
	    
	  //Constructeur par d�faut
	  public Capitale(){
	    //Ce mot cl� appelle le constructeur de la classe m�re
	    super();

	  }    
	      
	  //Constructeur d'initialisation de capitale
	  public Capitale(String nom, int hab, String pays, String monu){
	    super(nom, hab, pays);

	  }    
	     
	  public String decrisToi(){
	    String str = super.decrisToi();

	    return str;
	    } 

	  
	}