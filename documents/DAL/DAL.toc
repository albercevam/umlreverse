\select@language {french}
\contentsline {section}{\numberline {1}Objectif}{4}{section.1}
\contentsline {section}{\numberline {2}Documents applicables de r\IeC {\'e}f\IeC {\'e}rence}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Expression du besoin du projet UML Reverse}{4}{subsection.2.1}
\contentsline {section}{\numberline {3}Lexique}{4}{section.3}
\contentsline {section}{\numberline {4}Configuration requise}{4}{section.4}
\contentsline {section}{\numberline {5}Architecture statique}{5}{section.5}
\contentsline {subsection}{\numberline {5.1}Vue succinte de l'architecture et des modifications qui seront r\IeC {\'e}alis\IeC {\'e}es}{6}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Paquetage model}{6}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}El\IeC {\'e}ments li\IeC {\'e}s}{6}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Diagramme de classe}{6}{subsubsection.5.2.2}
\contentsline {subsection}{\numberline {5.3}Paquetage ui}{7}{subsection.5.3}
\contentsline {subsubsection}{\numberline {5.3.1}La partie gauche}{8}{subsubsection.5.3.1}
\contentsline {subsubsection}{\numberline {5.3.2}La partie centrale}{8}{subsubsection.5.3.2}
\contentsline {subsubsection}{\numberline {5.3.3}\IeC {\'E}diteur de diagramme}{9}{subsubsection.5.3.3}
\contentsline {subsection}{\numberline {5.4}Paquetage main}{9}{subsection.5.4}
\contentsline {section}{\numberline {6}Fonctionnement dynamique}{10}{section.6}
\contentsline {subsection}{\numberline {6.1}Cr\IeC {\'e}er un diagramme (DIA\_10, DIA\_20, DIA\_30)}{10}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}G\IeC {\'e}n\IeC {\'e}rer un diagramme de classe en reverse ingineering par introspection java (DIA\_40)}{10}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Exporter/Importer un diagramme (IHM\_30, IHM\_40)}{11}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}\IeC {\'E}diter un diagramme de paquetage (PAQ\_X)}{14}{subsection.6.4}
\contentsline {section}{\numberline {7}Tra\IeC {\c c}abilit\IeC {\'e}}{16}{section.7}
\contentsline {paragraph}{Mod\IeC {\`e}le}{16}{section*.5}
\contentsline {paragraph}{Partie gauche de l'application}{16}{section*.6}
\contentsline {paragraph}{IDiagramEditor}{16}{section*.7}
