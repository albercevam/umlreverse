package fr.univrouen.umlreverse.ui.component.packag.elements;

import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewPackage;

public interface IPackageGraphic {
	static final int POSITION_TITLE = -20;
	static final int POSITION_RECT_TITLE = -22;

	IViewPackage getViewPackage();
}
