package fr.univrouen.umlreverse.ui.component.packag.elements;

import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewEntity;
import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewPackage;
import fr.univrouen.umlreverse.ui.component.clazz.elements.IObjectEntityGraphic;
import fr.univrouen.umlreverse.ui.component.common.elements.AEntityTextGraphic;
import fr.univrouen.umlreverse.ui.view.common.IDiagramEditorController;
import fr.univrouen.umlreverse.ui.view.packag.PackageController;
import javafx.geometry.VPos;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

public class PackageGraphic extends AEntityTextGraphic implements IPackageGraphic, IObjectEntityGraphic {

	// ATTRIBUTS
	private PackageGraphicController controller;
	private final Rectangle rect;
	private final Rectangle rectTitle;

	// CONSTRUCTEUR
	public PackageGraphic(PackageController diagram, IViewPackage pack) {
		super(diagram);

		Text text = getText();
		text.setTextOrigin(VPos.BOTTOM);
		text.setTextAlignment(TextAlignment.LEFT);

		rectTitle = new Rectangle();
		rect = new Rectangle();

		rectTitle.setStroke(Color.web(IDiagramEditorController.COLOR_STROK_SHAPE));
		rect.setStroke(Color.web(IDiagramEditorController.COLOR_STROK_SHAPE));

		rectTitle.setTranslateY(POSITION_RECT_TITLE);
        text.setTranslateY(POSITION_TITLE);

		Pane p = new Pane();
		p.getChildren().add(rect);
        p.getChildren().add(rectTitle);
        p.getChildren().add(text);
        p.toBack();
        setCenter(p);

		controller = new PackageGraphicController(this, diagram, pack);
		// TODO

	/*	positionProperty().addListener(new ChangeListener<Point2D>() {

			@Override
			public void changed(ObservableValue<? extends Point2D> observable, Point2D oldValue, Point2D newValue) {
				getController().getModel().getPackages().forEach(pack -> {
					getController().refresh();

				});

				getController().getModel().getEntities().forEach(rel -> {
					RelationToObjectGraphic relG = getController().getDiagramController().get().get(rel);
					System.out.println(rel);
					relG.getController().setTranslatePosition(new Point2D(relG.getTranslateX(), newValue.getY() +
							relG.getTranslateY() - oldValue.getY()));
				});
				getController().getModel().getPackages().forEach(pack -> {
					double valueX = getController().getDiagramController().getPackages().get(pack).getTranslateX();
					double valueY = newValue.getY() + getController().getDiagramController().getPackages().get(pack).getTranslateY() - oldValue.getY();
					getController().getDiagramController().getPackages().get(pack).getController().setTranslatePosition(new Point2D(valueX, valueY));
				});

			}

        });*/
        autosize();
	}

	@Override
	public IPackageGraphicController getController() {
		return controller;
	}

	@Override
    public IViewPackage getViewPackage() {
        return controller.getModel();
    }

	public Rectangle getRectangleTitle() {
		return rectTitle;
	}

	public Rectangle getRectangle() {
		return rect;
	}

	@Override
	public int getTextSize() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public IViewEntity getViewEntity() {
		return (IViewEntity) controller.getModel();
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub

	}
}
