package fr.univrouen.umlreverse.ui.component.packag.elements;

import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewPackage;
import fr.univrouen.umlreverse.model.diagram.util.IStyle;
import fr.univrouen.umlreverse.ui.component.common.elements.AEntityGraphicController;
import fr.univrouen.umlreverse.ui.component.common.elements.IEntityTextGraphicController;
import fr.univrouen.umlreverse.ui.component.packag.dialog.DialogOPackage;
import fr.univrouen.umlreverse.ui.view.common.IDiagramEditorController;
import fr.univrouen.umlreverse.ui.view.packag.IPackageController;
import fr.univrouen.umlreverse.ui.view.packag.PackageController;
import fr.univrouen.umlreverse.util.Contract;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

public class PackageGraphicController extends AEntityGraphicController
implements IEntityTextGraphicController, IPackageGraphicController {

	private final PackageGraphic packageG;
	private final IViewPackage model;
	private final PackageController diagramController;

	public PackageGraphicController(PackageGraphic packageGraphic, PackageController diagram, IViewPackage packag) {
		 super();
        Contract.check(packageGraphic != null,
                 "L'argument noteG ne doit pas être nul.");
        Contract.check(diagram != null,
                 "L'arguments diagramController ne doit pas être nul.");
        Contract.check(packag != null,
                 "L'argument packag ne doit pas être nul.");

        this.diagramController = diagram;
        this.model = packag;
        this.packageG = packageGraphic;
        setText();
        IStyle style = model.getStyle();
        setStyle(style, true);
        createController();
	}

	// REQUEST
    @Override
    public Color getBackgroundColor() {
        IStyle style = model.getStyle();
        String styleBackgroundColor = style.getValue(IDiagramEditorController.BACKGROUND_COLOR_STYLE_ID);
        return Color.web(styleBackgroundColor);
    }

    @Override
    public IViewPackage getModel() {
        return model;
    }

    @Override
    public Color getTextColor() {
        IStyle style = model.getStyle();
        String styleColor = style.getValue(IDiagramEditorController.TEXT_COLOR_STYLE_ID);
        return Color.web(styleColor);
    }

    @Override
    public IPackageController getDiagramController() {
    	return diagramController;
    }

// COMMANDS

    @Override
    public void refresh() {
    	setText();
    }

    @Override
    public void loadRelations() {
    }

    @Override
    public final void setText(String s) {
        Contract.check(s != null, "L'argument text ne doit pas être nul.");
		model.setName(s);
    }

    @Override
    public void setBackgroundColor(Color c) {
        Contract.check(c != null, "L'argument c ne doit pas être nul.");
        model.addStyle(IDiagramEditorController.BACKGROUND_COLOR_STYLE_ID, c.toString());
    }

    @Override
    public void setTextColor(Color c) {
        Contract.check(c != null, "L'argument c ne doit pas être nul.");
        model.addStyle(IDiagramEditorController.TEXT_COLOR_STYLE_ID, c.toString());
    }

    @Override
    public void setPosition(Point2D value) {
    	Contract.check(value != null, "L'argument value "
                + "ne doit pas être nul.");
        IStyle style = model.getStyle();
        String position = style.getValue(IDiagramEditorController.POSITION_STYLE_ID);
        String[] positionTab = position.split("\\|");

        double tX = Double.valueOf(positionTab[2]);
        double tY = Double.valueOf(positionTab[3]);

        model.addStyle(IDiagramEditorController.POSITION_STYLE_ID,
                value.getX() + "|" + value.getY() + "|"
                + tX + "|" + tY);
        positionProperty().set(value);
    }

    @Override
    public void setTranslatePosition(Point2D value) {
    	 Contract.check(value != null, "L'argument value "
                 + "ne doit pas être nul.");
         IStyle style = model.getStyle();
         String position = style.getValue(IDiagramEditorController.POSITION_STYLE_ID);
         double lX = 0;
         double lY = 0;
         if (position != null) {
             String[] positionTab = position.split("\\|");
             lX = Double.valueOf(positionTab[0]);
             lY = Double.valueOf(positionTab[1]);
         }

         model.addStyle(IDiagramEditorController.POSITION_STYLE_ID,
                 lX + "|" + lY + "|"
                 + value.getX() + "|" + value.getY());

         positionProperty().set(new Point2D(value.getX() + lX, value.getY() + lY));
    }

    // CONTROLLER
    private void createController() {
        final ContextMenu ctxMenu = getContextMenu();
        // TODO
        MenuItem editMI = getEditMI();
        MenuItem removeMI = getRemoveMI();

        // Show context menu.
        packageG.addEventHandler(ContextMenuEvent.CONTEXT_MENU_REQUESTED,
            new EventHandler<ContextMenuEvent>() {
                @Override
                public void handle(ContextMenuEvent event) {
                	packageG.setSelected(true);
                    ctxMenu.show(packageG, event.getScreenX(), event.getScreenY());
                }
            }
        );

        // Show DialogNoteEdit when a double click is detected.
        packageG.addEventFilter(MouseEvent.MOUSE_CLICKED,
                new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    edit();
                }
            }
        });

        editMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                edit();
            }
        });

        removeMI.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                diagramController.removePackage(model);
            }
        });

        model.addPropertyChangeListener(IViewPackage.NAME_CHANGED_PROPERTY_NAME, evt -> {
            setText();
        });

        //TODO pour le père
      /*  model.addPropertyChangeListener(IViewPackage.NAME_CHANGED_PROPERTY_NAME, evt -> {
            setText();
        });*/

        model.addPropertyChangeListener(IViewPackage.STYLE_CHANGED_PROPERTY_NAME, evt -> {
            setStyle((IStyle) evt.getNewValue(), false);
        });

 /*       diagramController.getObjects().get(model.getStart()).positionProperty().addListener(new ChangeListener<Point2D>() {

			@Override
			public void changed(ObservableValue<? extends Point2D> observable, Point2D oldValue, Point2D newValue) {
				Point2D srcPoint = diagramController.getObjects().get(model.getStart()).lifeLineStartPosition();
				Point2D dstPoint = diagramController.getObjects().get(model.getEnd()).lifeLineStartPosition();
				double width = Math.abs(srcPoint.getX()
						- dstPoint.getX());
		        segmentG.getRectangle().setWidth(width + 150 - 20 * model.getLevel());
				if (srcPoint.getX() < dstPoint.getX()) {
					segmentG.setTranslateX(srcPoint.getX() - 40 + 10 * model.getLevel());
				}

			}
		});
*/
 /*       diagramController.getObjects().get(model.getEnd()).positionProperty().addListener(new ChangeListener<Point2D>() {

			@Override
			public void changed(ObservableValue<? extends Point2D> observable, Point2D oldValue, Point2D newValue) {
				Point2D srcPoint = diagramController.getObjects().get(model.getStart()).lifeLineStartPosition();
				Point2D dstPoint = diagramController.getObjects().get(model.getEnd()).lifeLineStartPosition();
				double width = Math.abs(srcPoint.getX()
						- dstPoint.getX());
		        segmentG.getRectangle().setWidth(width + 150 - 20 * model.getLevel());
				if (srcPoint.getX() > dstPoint.getX()) {
					segmentG.setTranslateX(dstPoint.getX() - 40);
				}
			}
		});
*/
    }

// PRIVATE
    /**Relation
     * Show DialogEditNote to edit NoteGraphic.
     * @param
     */
    private void edit() {
        DialogOPackage dialog = new DialogOPackage(diagramController, model);
        dialog.showAndWait();
    }

    private void setText() {
    	packageG.getText().setText(model.getName());

    	packageG.getText().setX(5);
    	packageG.getText().setY(15);
    	packageG.getRectangleTitle().setHeight(packageG.getText().getLayoutBounds().getHeight()
                + 5);
    	packageG.getRectangleTitle().setWidth(packageG.getText().getLayoutBounds().getWidth()
                + 10);
    /*    if (!model.getName().equals("")) {
        	packageG.getText().setText(model.getName());
        }*/
        double height = 200;
        double width = 350;
 /*       for (ISegment seg : model.getSegments()) {
        	height = height + diagramController.getSegments().get(seg).getRectangle().getHeight();
        }
        height = height + 20 * model.getRelations().size();
*/
        packageG.getRectangle().setHeight(height);
		packageG.getRectangle().setWidth(width /*model.getLevel()*/);
		packageG.autosize();
    }

    private void setStyle(IStyle style, boolean inConstructor) {
    	packageG.getRectangle().setFill(Color.TRANSPARENT);
    	packageG.getRectangleTitle().setFill(Color.web(IDiagramEditorController.BACKGROUND_COLOR_DEFAULT_ENTITY));
    	packageG.getText().setFill(Color.web(IDiagramEditorController.TEXT_COLOR_DEFAULT));

        String position = style.getValue(IDiagramEditorController.POSITION_STYLE_ID);
        String[] positionTab = position.split("\\|");
        double lX = Double.valueOf(positionTab[0]);
        double lY = Double.valueOf(positionTab[1]);
        double tX = Double.valueOf(positionTab[2]);
        double tY = Double.valueOf(positionTab[3]);

        packageG.setLayoutX(lX);
        packageG.setLayoutY(lY);
        packageG.setTranslateX(tX);
        packageG.setTranslateY(tY);

        if (inConstructor) {
            positionProperty().setValue(new Point2D(tX + lX, tY + lY));
        }
    }

}
