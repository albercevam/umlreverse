package fr.univrouen.umlreverse.ui.component.sequence.elements;

import fr.univrouen.umlreverse.ui.component.common.elements.IEntityRelationGraphic;

public interface IActivityGraphic extends IEntityRelationGraphic {
	static final int WIDTH = 10;
	static final int HEIGHT = 30;
}
