package fr.univrouen.umlreverse.ui.component.packag.dialog;

import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewPackage;
import fr.univrouen.umlreverse.model.diagram.clazz.view.ViewPackage;
import fr.univrouen.umlreverse.model.project.IObjectEntity;
import fr.univrouen.umlreverse.model.project.ObjectEntity;
import fr.univrouen.umlreverse.model.project.TypeEntity;
import fr.univrouen.umlreverse.model.project.Visibility;
import fr.univrouen.umlreverse.model.util.RefusedAction;
import fr.univrouen.umlreverse.ui.view.common.IDiagramEditorController;
import fr.univrouen.umlreverse.ui.view.packag.IPackageController;
import fr.univrouen.umlreverse.util.Contract;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * OEG's dialog controller. Uses resources/fxml/DialogOPackage.fxml
 */
public class DialogOPackageController {

    /** The package being created. */
    private IViewPackage entity;

    private IPackageController controller;

    @FXML
    private TextField entityName;
    @FXML
    private ColorPicker color;

    /**
     * The package being created.
     * @return the package being created
     */
    public IViewPackage getViewPackage() {
        return entity;
    }

    /**
     * Initializes fields and events.
     * @param diagramController the diagram used.
     * @param typeEntity the type wanted.
     */
    public final void setBehaviors(IPackageController diagramController) {
        Contract.check(diagramController != null, "diagramController must not be null");
        Platform.runLater(() -> entityName.requestFocus());
        this.controller = diagramController;
        // TODO pour les pères
        if (this.entity != null) {
			this.entityName.setText(entity.getName());
		} else {
			color.setValue(Color.web(IDiagramEditorController.TEXT_COLOR_DEFAULT));
		}
    }

    /**
     * Add the entity into the diagram from the user input.
     * @throws RefusedAction if there is conflicts between entities.
     */
    public final void addEntity() {
    	// Nouvelle entité
    	String name = entityName.getText();

    	TypeEntity typeEntity = TypeEntity.Packag;
        Visibility visibility = Visibility.Private;

		IObjectEntity objectEntity =
				new ObjectEntity(name, typeEntity, visibility);

		try {
			controller.getDiagram().getProject().addEntity(objectEntity);
		} catch (RefusedAction e) {
			e.printStackTrace();
		}

    	// TODO pour les pères
    	if (entity == null) {
    		 entity = new ViewPackage(objectEntity, name, controller.getDiagram());
		} else {
			entity.setName(name);
		}

        entity.addStyle(IDiagramEditorController.TEXT_COLOR_STYLE_ID,
        		toRGBCode(color.getValue()));
    }

    public void setPackage(IViewPackage pack) {
		this.entity = pack;
	}

    // STATIC PRIVATE TOOLS
    /**
     * @param color the color.
     * @return the rgb web code corresponding to this color.
     */
    private static String toRGBCode(Color color) {
        return String.format("#%02X%02X%02X",
                (int) (color.getRed() * 255),
                (int) (color.getGreen() * 255),
                (int) (color.getBlue() * 255));
    }
}
