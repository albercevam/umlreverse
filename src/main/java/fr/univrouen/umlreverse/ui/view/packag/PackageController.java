package fr.univrouen.umlreverse.ui.view.packag;

import java.util.HashMap;
import java.util.Map;

import fr.univrouen.umlreverse.model.diagram.clazz.view.INoteClass;
import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewEntity;
import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewPackage;
import fr.univrouen.umlreverse.model.diagram.packag.IPackageDiagram;
import fr.univrouen.umlreverse.ui.component.clazz.elements.ObjectEntityGraphic;
import fr.univrouen.umlreverse.ui.component.packag.dialog.DialogOPackage;
import fr.univrouen.umlreverse.ui.component.packag.elements.IPackageGraphic;
import fr.univrouen.umlreverse.ui.component.packag.elements.PackageGraphic;
import fr.univrouen.umlreverse.ui.view.clazz.ClassController;
import fr.univrouen.umlreverse.ui.view.common.ADiagramEditor;
import fr.univrouen.umlreverse.ui.view.common.IDiagramEditorController;
import fr.univrouen.umlreverse.util.Contract;
import fr.univrouen.umlreverse.util.ErrorDialog;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;

public class PackageController extends ClassController implements IPackageController {

	// ATTRIBUTES
     /**
     * The menu item that will allow the creation of a class/interface/enum/abstract.
     */
    private MenuItem createPackageMI;

    /**
     * A map that will contain all package, the model of package as key and the graphic as value.
     */
    private final Map<IViewPackage, PackageGraphic> packages;

    /**
     * An object property for the creation of a relation.
     */
    private final ObjectProperty<IViewEntity> createPackageRelation;

    /**
     * The package's relation handler.
     */
    private EventHandler<MouseEvent> addPackageRelationEvt;

// CONSTRUCTORS

    /**
     * Constructor of the class controller.
     * @param editor the ADiagramEditor which will be correlated whith the controller
     * @param classDiagram the classDiagram which will be correlated with the controller
     */
    public PackageController(ADiagramEditor editor, IPackageDiagram packageDiagram) {
        super(editor, packageDiagram);
        packages = new HashMap<>();

        createPackageRelation = new SimpleObjectProperty<>();
    }

// REQUESTS
    @Override
    public IPackageDiagram getDiagram() {
        return (IPackageDiagram) diagram;
    }

    @Override
    public PackageGraphic getPackage(IViewPackage packag) {
        Contract.check(packag != null, "L'argument entity ne doit pas être nul.");
        return packages.get(packag);
    }

    @Override
	public void createPackage(double x, double y) {
        DialogOPackage dialogOPackage = new DialogOPackage(PackageController.this);
        dialogOPackage.showAndWait();
        IViewPackage entity = dialogOPackage.getViewPackage();
        entity.addStyle(IDiagramEditorController.POSITION_STYLE_ID, x + "|" + y + "|10|0");

        getDiagram().addPackage(entity);
    }

    @Override
    public void createPackageRelation(IViewPackage packag) {
    	//TODO creer les relations entre les packages
    }

    @Override
    public void removePackage(IViewPackage packag) {
    	Contract.check(packag != null, "The packag must not be null.");
	    diagram.removePackage(packag);
	    packages.remove(packag);
    }

// CONTROLLER
    /**
     * Add controllers on DiagramEditor
     */
    protected void createController() {
        createContextMenu();
        createButtonController();
        createModelListeners();
        createPropertyListeners();
    }

    /**
     * Create controller for the menu item of the context menu.
     */
    protected void createButtonController() {
    	super.createButtonController();

        createPackageMI.setOnAction(actionEvent -> {
            createPackage(miceX, miceY);
            deselectEntity();
        });
    }

    /**
     * Create listener that will listen to the model.
     */
    protected void createModelListeners() {
    	super.createModelListeners();

        diagram.addPropertyChangeListener(IPackageDiagram.PACKAGE_ADDED_PROPERTY_NAME, evt -> {
            IViewPackage entity = (IViewPackage) evt.getNewValue();
            createPackageGraphic(entity);
        });

        diagram.addPropertyChangeListener(IPackageDiagram.PACKAGE_REMOVED_PROPERTY_NAME, evt -> {
        	IViewPackage packag = (IViewPackage) evt.getOldValue();
            IPackageGraphic packageG = packages.get(packag);
            packages.remove(packag);
            editor.getCanvas().getChildren().remove(packageG);
        });

    /*    diagram.addPropertyChangeListener(RELATION_ADDED_PROPERTY_NAME, evt -> {
            IViewPackage vp = (IViewPackage) evt.getNewValue();
      //      createRelationGraphic(vp);
        });*/
    }

    /**
     * Create a graphic relation that will correspond to the IViewRelation given in parameter.
     * @param vr the model of the graphic relation that will be created
     */
  /*  protected void createRelationGraphic(IViewPackage vp) {
        try {
            IRelationGraphic rg = new RelationGraphic(this, vp,
                    viewEntities.get(objectEntities.get(vp.getEntitySource())),
                    viewEntities.get(objectEntities.get(vp.getEntityTarget())));
            rg.drawInGroup(editor.getCanvas());
            viewRelations.put(vr, rg);
        } catch (IllegalArgumentException ex) {

        }
    }
*/
    /**
     * Create Property Change listener that will observe the creation of new Entity.
     */
    protected void createPropertyListeners() {
    	super.createPropertyListeners();

        createNoteRelation.addListener(new ChangeListener<INoteClass>() {
            @Override
            public void changed(ObservableValue<? extends INoteClass> observable,
                    INoteClass oldValue, INoteClass newValue) {
                addNoteRelationEvt = new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            if (event.getSource().getClass() == ObjectEntityGraphic.class) {
                                ObjectEntityGraphic oeg = (ObjectEntityGraphic) event.getSource();
                                newValue.addEntity(oeg.getViewEntity());
                            } else if (event.getSource().getClass() == PackageGraphic.class) {
                            	 PackageGraphic pg = (PackageGraphic) event.getSource();
                                 newValue.addEntity(pg.getViewEntity());
                            } else {
                    			ErrorDialog.showError("Erreur",
                    					"Vous ne pouvez relier une note qu'à "
                    							+ "une entité (Interface/Classe/"
                    							+ "Enumeration/Paquetage)");
                            }
                            removeMouseEvent(addNoteRelationEvt);
                            event.consume();
                        }
                };
            }
        });

        // TODO creer les relations entre les paquetages
        //createPackageRelation.
    }

    /**
     * To instancie new ContextMenu and MenuItems
     */
    protected void createComposant() {
    	super.createComposant();

        createPackageMI = new MenuItem("Créer un paquetage");
        ctxMenuDiagram.getItems().add(createPackageMI);
    }


// PRIVATES

    /**
     * Add mouse listener to Entity.
     * @param event the Event Handler that will be add
     */
    protected void addMouseEvent(EventHandler<MouseEvent> event) {
    	super.addMouseEvent(event);

        packages.values().stream().forEach((packageG) -> {
        	packageG.addEventFilter(MouseEvent.MOUSE_CLICKED, event);
        });
    }

    /**
     * Create a graphic relation that will correspond to the IViewRelation given in parameter.
     * @param vr the model of the graphic relation that will be created
     */
	private void createPackageGraphic(IViewPackage packag) {
		PackageGraphic packageG = new PackageGraphic(this, packag);
        packages.put(packag, packageG);
        packageG.addEventHandler(MouseEvent.MOUSE_PRESSED,
                nodeGestures.getOnMousePressedEventHandler());
        packageG.addEventHandler(MouseEvent.MOUSE_DRAGGED,
                nodeGestures.getOnMouseDraggedEventHandler());
        editor.getCanvas().getChildren().add(packageG);
    }

    /**
     * Draw model in editor.
     */
    protected void drawDiagram() {
    	super.drawDiagram();

        diagram.getPackages().stream().forEach((packag) -> {
        	createPackageGraphic(packag);
        });
    }

    /**
     * Remove mouse listener of all Entities.
     * @param event the event handler that will be remove
     */
    protected void removeMouseEvent(EventHandler<MouseEvent> event) {
    	super.removeMouseEvent(event);

        packages.values().stream().forEach((packageG) -> {
        	packageG.addEventFilter(MouseEvent.MOUSE_CLICKED, event);
        });
    }
}
