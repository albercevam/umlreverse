package fr.univrouen.umlreverse.model.diagram.clazz.view;

import java.io.Serializable;
import java.util.Set;

import fr.univrouen.umlreverse.model.diagram.clazz.visitor.IClassVisitor;
import fr.univrouen.umlreverse.model.diagram.common.IObservable;
import fr.univrouen.umlreverse.model.diagram.common.IStylizable;

/**
 * A view on a package
 */
public interface IViewPackage extends Serializable, IStylizable, IObservable {

	String PACKAGE_STYLE_ID = "package";

	String NAME_CHANGED_PROPERTY_NAME = "NameChanged";

	String PACKAGE_ADDED_PROPERTY_NAME = "PackageAdded";
	String PACKAGE_REMOVED_PROPERTY_NAME = "PackageRemoved";

    String PARENT_CHANGED_PROPERTY_NAME = "ParentChanged";

	String STYLE_CHANGED_PROPERTY_NAME = "StyleChanged";

    /**
     * The diagram
     */
    IClassDiagram getDiagram();

    /**
     * getStyleId Getter that permits to give the id of the style
     * @return String
     */
    String getStyleId();

    /**
     * getter of name
     * @return the name
     */
    String getName();

    /**
     * The parent folder
     */
    IViewPackage getPackage();

    /**
     * The specific id of package for packages style ids
     */
	String getId();

    /**
     * The absolute name
     */
    String getAbsoluteName();

    /**
     * getter of packages
     * @return the packages
     */
    Set<IViewPackage> getPackages();

    /**
     * getter of all the entities
     * @return the listEntities
     */
    Set<IViewEntity> getEntities();

    // METHODS

    /**
     * addPackage allows to add packages
     */
    void addPackage(IViewPackage viewPackage);

    /**
     * removePackage allows to remove packages
     */
    void removePackage(IViewPackage viewPackage);

    /**
     * addEntity allows to add entities
     * @param entity
     * 		the entity to add
     */
    void addEntity(IViewEntity entity);

    /**
     * removeEntity allows to remove entities
     * @param entity
     * 		the entity to remove
     */
    void removeEntity(IViewEntity entity);

    /**
     * setter of parent
     */
    void setPackage(IViewPackage parent);

    /**
     * setter of name
     * @param namePackage the name to set
     */
    void setName(String namePackage);

    /**
     * visitor
     * @param visitor
     */
    void accept(IClassVisitor visitor);
}
