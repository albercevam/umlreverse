package fr.univrouen.umlreverse.model.diagram.clazz.visitor;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import fr.univrouen.umlreverse.model.diagram.clazz.view.ClassDiagram;
import fr.univrouen.umlreverse.model.diagram.clazz.view.IClassDiagram;
import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewEntity;
import fr.univrouen.umlreverse.model.diagram.clazz.view.IViewPackage;
import fr.univrouen.umlreverse.model.util.ErrorAbstraction;
import fr.univrouen.umlreverse.util.Contract;

public class ExportXMIClassVisitor extends AbstractClassVisitor {

	 	private File file;
	    private String error = null;
	    private Map<String, Integer> toId = new HashMap<>();

	    public ExportXMIClassVisitor(String filename) {
	        this(new File(filename));
	    }
	    public ExportXMIClassVisitor(File file) {
	        Contract.check(file != null);
	        this.file = file;
	    }

	    @Override
	    public void visit(ClassDiagram diagram) {
	        file.delete();
	        try {
				analyse(diagram);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    }

	    public String getError() {
	        return error;
	    }
	    
	    private void analyse(IClassDiagram diagram) throws IOException {
	        error = null;
	        BufferedWriter writer;
	        try {
	            writer = new BufferedWriter(new FileWriter(file));
	        } catch (IOException e) {
	            error = ErrorAbstraction.ErrorImpossibleToCreateFile.getCode();
	            return;
	        }

	        /******************************
	         * Code de l'écriture du fichier XMI
	         ******************************/
	        
	        writer.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
	        writer.append("<XMI xmi.version=\"1.2\" xmlns:UML=\"org.omg/UML/1.4\">\n");
	        writer.append("<XMI.header>\n");
	        writer.append("<XMI.documentation> </XMI.documentation>\n");
	        writer.append("<XMI.metamodel xmi.name=\"UML\" xmi.version=\"1.4\"/>\n");
	        writer.append("</XMI.header>\n");
	        writer.append("<XMI.content>\n");
	        
	        try {
				writePackagesAndEntities(diagram, writer);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        
            try {
				writeRelations(diagram, writer);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
            
            try {
				writeNotes(diagram, writer);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	        
	        writer.append("</XMI.content>\n");
	        writer.append("</XMI>\n");
	        
	        try {
	            writer.close();
	        } catch (IOException e) {
	            error = ErrorAbstraction.ErrorImpossibleToCloseTheFile.getCode();
	        }
	    }
	    
	    
	    private void writeNotes(IClassDiagram diagram, BufferedWriter writer) throws IOException {
	     	
	    }

	    private void writeRelations(IClassDiagram diagram, BufferedWriter writer) throws IOException {
	        
	    }

	    private void writePackagesAndEntities(IClassDiagram diagram, BufferedWriter writer) throws IOException {
	    	for (IViewPackage p : diagram.getPackages()) {
	            writePackage(writer, p);
	        }
	        for (IViewEntity entity : diagram.getEntities()) {
	            writeEntities(entity, writer);
	        }
	    }
	    
	    private void writeEntities(IViewEntity entity, BufferedWriter writer) throws IOException {
	    	
	    	 // open entity
	        String type = "?(TypeEntity)";
	        switch (entity.getType()) {
	            case Abstract:
	                type = "abstract class";
	                break;
	            case Clazz:
	                type = "class";
	                break;
	            case Interface:
	                type = "interface";
	                break;
	            case Enumeration:
	                type = "enum";
	                break;
	        }
	        
	        String name = entity.getAbsoluteName();
	        if (!toId.containsKey(name))
	            toId.put(name, toId.size());
	        writer.write("<Foundation.Core.Class xmi.id=\"Class:"+ entity.getAbsoluteName() +"\">\n");
	        writer.write("<Foundation.Core.ModelElement.name>" + entity.getAbsoluteName() + "</Foundation.Core.ModelElement.name>\n");
	        writer.write("</Foundation.Core.Class>\n");
	        
	       
	        	/*
		        <Foundation.Core.Class xmi.id="Class:Class1">
		            <Foundation.Core.ModelElement.name>Class4</Foundation.Core.ModelElement.name>
		            <Foundation.Core.ModelElement.visibility xmi.value="public"/>
		            <Foundation.Core.ModelElement.isSpecification xmi.value="false"/>
		            <Foundation.Core.GeneralizableElement.isRoot xmi.value="true"/>
		            <Foundation.Core.GeneralizableElement.isLeaf xmi.value="true"/>
		            <Foundation.Core.GeneralizableElement.isAbstract xmi.value="false"/>
		            <Foundation.Core.Class.isActive xmi.value="false"/>
		            
		            <Foundation.Core.ModelElement.namespace>
		               <Foundation.Core.Namespace xmi.idref="Model:example"/>
		            </Foundation.Core.ModelElement.namespace>
		            
		            <Foundation.Core.Attribute xmi.id="Attribute:att1">
	                	<Foundation.Core.ModelElement.name>att1</Foundation.Core.ModelElement.name>
	                	<Foundation.Core.ModelElement.visibility xmi.value="private"/>
	                	<Foundation.Core.ModelElement.isSpecification xmi.value="false"/>
	                </Foundation.Core.Attribute>
	                
	                <Foundation.Core.StructuralFeature.type>
	                	<Foundation.Core.Classifier xmi.idref="DataType:decimal"/>
	                </Foundation.Core.StructuralFeature.type>
	                
	            </Foundation.Core.Class>
	            */
	        
	        
	    }
	    
	    private void writePackage(BufferedWriter writer, IViewPackage p) throws IOException {
	    	
	    }
}


