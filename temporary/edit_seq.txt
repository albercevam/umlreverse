title Editer un diagramme de séquence

alt isSelected //Si un diagramme de séquence est ouvert
    opt Créer un nouveau objet
        Utilisateur->Vue: createObject()
        Vue->Contrôleur: createObject(double, double)
		Vue->Utilisateur: showAndWait()
        Contrôleur->Modèle: addEntity()
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end    
    opt Supprimer un objet
        Utilisateur->Vue: deleteObject()a
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: removeObject(obj)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end    
    opt Créer une note
        Utilisateur->Vue: createNewNote(Text)
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: addNote()
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end    
    opt Modifier le texte d'une note
        Utilisateur->Vue: setTextNote(text)
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: setText(text)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
    opt Créer une relation
        Utilisateur->Vue: createNewConnection()
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: addConnection(obj1, obj2, type)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
    opt Créer une nouvelle ligne de vie
        Utilisateur->Vue: createNewLifeline()
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: addLifeline(position, height)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
    opt Modifier une ligne de vie
        Utilisateur->Vue: modifyLifeline()
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: setLifeline(position, height)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end
    opt Créer un nouveau segment
        Utilisateur->Vue: createNewSection()
        Vue->Contrôleur: notify()
        Contrôleur->Vue: choiceTypeSection()
        Utilisateur->Vue: valid(text, type)
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: addSection(type, text)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end
    opt Modifier le texte d'un segment
        Utilisateur->Vue: setTextSection(text)
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: setText(text)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
    opt Supprimer une entité (autre qu'un objet)
        Utilisateur->Vue: delete()
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: remove(entity)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
    opt Supprimer un segment
        Utilisateur->Vue: delete()
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: removeSection()
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
    opt Déplacer un segment
        Utilisateur->Vue: moveSection()
        Vue->Contrôleur: notify()
        Contrôleur->Modèle: setPosition(x, y, width, height)
        Modèle->Contrôleur: setChanged()
        Contrôleur->Vue: display()
    end   
end



title Editer un diagramme de séquence

alt isOpen() //Si un diagramme de séquence est ouvert
    opt Créer une nouvelle entité (Object, Note, Relation, Segment)
        Utilisateur->Vue: Button.NewObject
        Vue->Contrôleur: createObject(double, double)
		Contrôleur->Vue: showAndWait()
		Utilisateur->Vue: ButtonType.OK
        Vue->Contrôleur: addEntity()
        Contrôleur->+Modèle: new Object(IObjectEntity, String, ISequenceDiagram)
        Contrôleur->Modèle: getObject()
        Modèle-->-Contrôleur:
        Contrôleur->Vue: createObjectGraphic(IObject)
    end    
    opt Modifier une entité (Object, Note, Relation, Segment)
        Utilisateur->Vue: Button.UpdObject
        Vue->Contrôleur: notify()
        Contrôleur->+Modèle: setName(String)
        Modèle-->Contrôleur: firePropertyChange(String, old, new)
        Contrôleur->Modèle: setStereotype(String)
        Modèle-->-Contrôleur: firePropertyChange(String, old, new)
        Contrôleur->Vue: createObjectGraphic(IObject)
    end
    opt Supprimer une entité (Object, Note, Relation, Segment)
        Utilisateur->Vue: Button.DelObject
        Vue->Contrôleur: removeObject(IObject)
        Contrôleur->+Modèle: remove()
        Modèle-->-Contrôleur:
        Contrôleur->Vue: refresh()
    end 
end

